import { FuncUsuario } from "./funcUsu";
import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("SchemaUsuario")
export class SchemaUsuario {
    @JsonProperty('idRol', Number)
    rolId: number = undefined;
    @JsonProperty('rol', String)
    desRol: string = undefined;
    @JsonProperty('Funciones', [FuncUsuario])
    funciones: FuncUsuario[]  = undefined;
  }
