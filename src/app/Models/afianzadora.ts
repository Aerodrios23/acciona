import {JsonProperty, JsonObject} from "json2typescript";

@JsonObject("Afianzadora")
export class Afianzadora {
    @JsonProperty('CVE_AFIAN', Number)
    cveAfianz: number = undefined;
    @JsonProperty('NOM_AFIAN', String)
    nomAfianz: String = undefined;
    @JsonProperty('RFC_AFIAN', String)
    rfcAfianz: String = undefined;
    @JsonProperty('NOMBRE_LOGO', String)
    logoAfianz: String = undefined;
  }


