import {JsonProperty, JsonObject} from "json2typescript";
import { Client } from "./clienteToken";

@JsonObject("ClientData")
export class ClientData {
    @JsonProperty('Client', Client)
    Client: Client  = new Client();
    @JsonProperty('Parameters', String)
    Parameters: String = null;
  }