
import {JsonProperty, JsonObject, JsonConverter, JsonCustomConvert} from "json2typescript";
import { FilesSolNuevaDTO } from "./filesSolNuevaDTO";

@JsonObject("PostRegisterSolNueva")
export class PostRegisterSolNueva {
    @JsonProperty('userId', Number)
    userId:  number = undefined;
    @JsonProperty('userName', String)
    userName:  String = undefined;
    @JsonProperty('userDate', String)
    userDate:  String = undefined;
    @JsonProperty('ramoId', Number)
    ramoId:  number = undefined;
    @JsonProperty('subRamoId', Number)
    subRamoId:  number = undefined;
    @JsonProperty('paqId', Number)
    paqId:  number = undefined;
    @JsonProperty('tipMoviId', Number)
    tipMoviId:  number = undefined;
    @JsonProperty('clieId', Number)
    clieId:  number = undefined;
    @JsonProperty('afianId', Number)
    afianId:  number = undefined;
    @JsonProperty('gereId', Number)
    gereId:  number = undefined;
    @JsonProperty('nameBenef', Number)
    benfId:  number = undefined;
    @JsonProperty('benfId', String)
    nameBenef:  String = undefined;
    @JsonProperty('detSol', String)
    detSol:  String = undefined;
    @JsonProperty('filesSolNuevaDTO',  [FilesSolNuevaDTO])
    filesSolNuevaDTO: FilesSolNuevaDTO [] ;
    
  }

  @JsonObject("PostRegisterSolAct")
  export class PostRegisterSolAct {
      @JsonProperty('numSol', Number)
      numSol:  number = undefined;
      @JsonProperty('numFia', String)
      numFia:  number = undefined;
      @JsonProperty('idAfi', Number)
      idAfi:  number = undefined;
      @JsonProperty('dateVig', String)
      dateVig:  number = undefined;
      @JsonProperty('folioRec', String)
      folioRec:  number = undefined;
      @JsonProperty('dateRec', String)
      dateRec:  number = undefined;
      @JsonProperty('montoRec', Number)
      montoRec:  number = undefined;
      @JsonProperty('comRec', Number)
      comRec:  number = undefined;
      @JsonProperty('primaRec', Number)
      primaRec:  number = undefined;
      @JsonProperty('uuidRec', String)
      uuidRec:  number = undefined;
      @JsonProperty('relatFia', String)
      relatFia:  String = undefined;
      @JsonProperty('filesSolNuevaDTO',  [FilesSolNuevaDTO])
      filesSolNuevaDTO: FilesSolNuevaDTO [] ;
    }