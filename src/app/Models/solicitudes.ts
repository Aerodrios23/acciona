import {JsonProperty, JsonObject} from "json2typescript";

import {TipoSolicitud, TipoSolicitudNew} from './tipoSolicitud'
import {TipoMovimiento} from './tipoMov'
import { Afianzadora } from "./afianzadora";
import { Beneficiarios } from "./beneficiarios";
import { fileFianza } from "./fileNewSol";
import { TipoRamo, TipoSubRamo, Gerencias } from "./fiaCore";

@JsonObject("solicitudFianzasDTO")
export class solicitudFianzasDTO {

    @JsonProperty('NUM_SOL', String)
    numSol:         number = undefined
    @JsonProperty('CVE_CLIENTE', String)
    cveCli:     number = undefined 
    @JsonProperty('CVE_TIPO_SOL', String)
    cveTipSol:    number = undefined

    @JsonProperty('DES_TIPO_SOL', String)
    desTipSol: string = undefined;

    @JsonProperty('CVE_STATUS_SOL', String)
    cveStatusSol:  number = undefined

    @JsonProperty('DES_STATUS_SOL', String)
    desStSol: string = undefined;

    @JsonProperty('FEC_SOLICITUD', String)
    fecSol: string = undefined;
    @JsonProperty('FEC_MOVIMIENTO', String)
    fecMov: string = undefined;
    @JsonProperty('OBSERVACIONES_SOL', String)
    obsvSol:   string = undefined
    @JsonProperty('VALOR_REFERENCIA1', String)
    numFianza:   string = undefined
    @JsonProperty('VALOR_REFERENCIA2', String)
    numControl:   string = undefined

    @JsonProperty('CVE_USUARIO', String)
    cveUsuario:     number = undefined 
    @JsonProperty('DES_USUARIO', String)
    desUsuSol: string = undefined;
    
    
    @JsonProperty('CVE_SOLICITANTE', String)
    cveSol: number = undefined
    @JsonProperty('CVE_CORPORATIVO', String)
    cveCorporativo: number = undefined 
    @JsonProperty('CVE_GRUPO', String)
    cveGrupo:       number = undefined 
    @JsonProperty('CVE_BENEF', String)
    cveBenef:       number = undefined 
    @JsonProperty('CVE_OPERACION', String)
    cveOperacion:   string = undefined
    @JsonProperty('VALOR_REFERENCIA3', String)
    ordCompra:   string = undefined
    @JsonProperty('VALOR_REFERENCIA4', String)
    numReciboOrd:   string = undefined
    @JsonProperty('VALOR_REFERENCIA5', String)
    numInclusion:   string = undefined
    @JsonProperty('CVE_SUBTIPO_SOL', String)
    cveSubTipoSol:    number = undefined 
    @JsonProperty('ACTIVO', String)
    activo:             number = undefined 
    @JsonProperty('VALOR_REFERENCIA6', String)
    numReciboBim:  string = undefined



    //@JsonProperty('access_token', String)
    //accToken: string = undefined
   
  
}


JsonObject("solicitudNuevaDTO")
export class solicitudNuevaDTO {
    @JsonProperty('lstTipSolicitudes', [TipoSolicitud])
    lstTipSol:  TipoSolicitud [] = new Array<TipoSolicitud>()
    @JsonProperty('lstTipMovimiento', [TipoMovimiento])
    lstTipMovimiento:  TipoMovimiento [] = new Array<TipoMovimiento>()
    @JsonProperty('lstAfianzadoras', [Afianzadora])
    lstAfianzadoras:  Afianzadora [] = new Array<Afianzadora>()
    @JsonProperty('lstBeneficiarios', [Beneficiarios])
    lstBeneficiarios:  Beneficiarios [] = new Array<Beneficiarios>()
    
}

JsonObject("solicitudNuevaDTONew")
export class solicitudNuevaDTONew {
    
    @JsonProperty('lstTipRamos', [TipoSolicitud])
    lstTipRamos:  TipoRamo [] = new Array<TipoRamo>()
    @JsonProperty('lstTipSubRamos', [TipoSolicitud])
    lstTipSubRamos:  TipoSubRamo [] = new Array<TipoSubRamo>()
    @JsonProperty('lstTipSolicitudes', [TipoSolicitud])
    lstTipSol:  TipoSolicitudNew [] = new Array<TipoSolicitudNew>()
    @JsonProperty('lstTipMovimiento', [TipoMovimiento])
    lstTipMovimiento:  TipoMovimiento [] = new Array<TipoMovimiento>()
    @JsonProperty('lstAfianzadoras', [Afianzadora])
    lstAfianzadoras:  Afianzadora [] = new Array<Afianzadora>()
    @JsonProperty('lstBeneficiarios', [Beneficiarios])
    lstBeneficiarios:  Beneficiarios [] = new Array<Beneficiarios>()
    @JsonProperty('lstGerencias', [Gerencias])
    lstGerencias:  Gerencias [] = new Array<Gerencias>()
    
}





@JsonObject("solicitudAccionaFianzasDTO")
export class solicitudAccionaFianzasDTO {
    @JsonProperty('NUM_SOL', String)
    NUM_SOL:         number = undefined
    @JsonProperty('CVE_AFI', String)
    CVE_AFI:      number = undefined
    @JsonProperty('DES_AFI', String)
    DES_AFI:     number = undefined 
    @JsonProperty('CVE_CLIENTE', String)
    CVE_CLIENTE:     number = undefined 
    @JsonProperty('CVE_TIPO_SOL', String)
    CVE_TIPO_SOL:    number = undefined
    @JsonProperty('DES_TIPO_SOL', String)
    DES_TIPO_SOL: string = undefined;
    @JsonProperty('CVE_STATUS_SOL', String)
    CVE_STATUS_SOL:  number = undefined
    @JsonProperty('DES_STATUS_SOL', String)
    DES_STATUS_SOL: string = undefined;
    @JsonProperty('FEC_SOLICITUD', String)
    FEC_SOLICITUD: string = undefined;
    @JsonProperty('FEC_MOVIMIENTO', String)
    FEC_MOVIMIENTO: string = undefined;
    @JsonProperty('OBSERVACIONES_SOL', String)
    OBSERVACIONES_SOL:   string = undefined
    @JsonProperty('CVE_USUARIO', String)
    CVE_USUARIO:     number = undefined 
    @JsonProperty('DES_USUARIO', String)
    DES_USUARIO: string = undefined;
    @JsonProperty('CVE_SOLICITANTE', Number)
    CVE_SOLICITANTE: number = undefined
    @JsonProperty('DES_SOLICITANTE', String)
    DES_SOLICITANTE: string = undefined;
    @JsonProperty('CVE_CORPORATIVO', Number)
    CVE_CORPORATIVO: number = undefined 
    @JsonProperty('CVE_GRUPO', Number)
    CVE_GRUPO:       number = undefined 
    @JsonProperty('CVE_BENEF', Number)
    CVE_BENEF: number = undefined
    @JsonProperty('DES_BENEF', String)
    DES_BENEF: String = undefined
    @JsonProperty('CVE_SUBTIPO_SOL', Number)
    CVE_SUBTIPO_SOL:    number = undefined 
    @JsonProperty('DES_SUBTIPO_SOL', String)
    DES_SUBTIPO_SOL: String = undefined
    @JsonProperty('ACTIVO', Number)
    ACTIVO: number = undefined 
    @JsonProperty('NUM_FIA', String)
    NUM_FIA:   string = undefined
    @JsonProperty('PRIMA_FIA', Number)
    PRIMA_FIA: number = undefined 
    @JsonProperty('FEC_CON_FIA', String)
    FEC_CON_FIA:   string = undefined
    @JsonProperty('FEC_VIG_FIA', String)
    FEC_VIG_FIA:   string = undefined
    @JsonProperty('FEC_CUM_FIA', String)
    FEC_CUM_FIA:   string = undefined
    @JsonProperty('NUM_REC_FIA', String)
    NUM_REC_FIA:   string = undefined
    @JsonProperty('UUID_REC', String)
    UUID_REC:   string = undefined
    @JsonProperty('FEC_REC', String)
    FEC_REC:   string = undefined
    @JsonProperty('MON_REC', Number)
    MON_REC: number = undefined 
    @JsonProperty('COM_REC', Number)
    COM_REC: number = undefined 
    @JsonProperty('VALOR_REFERENCIA_1', String)
    VALOR_REFERENCIA1:   string = undefined
    @JsonProperty('lstFilesFia', [fileFianza])
    lstFilesFia:  fileFianza [] = new Array<fileFianza>()
    @JsonProperty('lstDocsFia', [fileFianza])
    lstDocsFia:  fileFianza [] = new Array<fileFianza>()
   
}