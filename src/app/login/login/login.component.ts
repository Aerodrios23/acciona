import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';

import { SessionStorageService, SessionStorage, StorageService } from 'angular-web-storage';
import {JsonConvert, JsonConverter, OperationMode, ValueCheckingMode} from "json2typescript"
import {NgxSpinnerService} from 'ngx-spinner'

import { Usuario } from 'src/app/Models/usuFront';
import { InfoUsuario } from 'src/app/Models/infoUser';
import { SchemaUsuario } from 'src/app/Models/shemaUsu';
import { ResponseUsu } from 'src/app/Models/respClient';

import { SecurityService } from 'src/app/Servicios/security.service';
import { DeviceDetectorService } from 'ngx-device-detector';

import * as moment from 'moment';


declare var $;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
      
    //Device info
    deviceInfo: any = null;

    loginForm: FormGroup;
    public isError = false;
    noFoundUsu:boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private securityServices: SecurityService,
        public session: SessionStorageService,
        private spinner: NgxSpinnerService,
        private deviceService: DeviceDetectorService,
    ) { }

  usuarioFront: Usuario = new Usuario() ;

  ngOnInit() {

      this.loginForm = this.formBuilder.group({
          inEmail: ['', Validators.compose([Validators.required, Validators.email]) ],
          inPass: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
          done: false
        });

  

      document.body.className = 'hold-transition login-page';
          $(() => {
              $('input').iCheck({
                  checkboxClass: 'icheckbox_square-blue',
                  radioClass: 'iradio_square-blue',
                  increaseArea: '20%' /* optional */
              });
          });
  }

 
  recContra(){
    alert("Entro al componente");
  }


  onLogin() {
    
    let usuFront = new Usuario ();
    let defUser = new InfoUsuario();
    let shemUser = new SchemaUsuario();
    let jsonCrt = new JsonConvert();

    
    if (this.loginForm.valid) {
      
      this.spinner.show();

      return this.securityServices
        .loginuser(this.loginForm.get('inEmail').value, this.loginForm.get('inPass').value)
        .subscribe(
        data => {
         var prueba = data;

            if (data) {
               
                //deserealizo obj Usu
                var objAllSt = JSON.stringify(data);
                //serealizo obj usu
                var objAll = JSON.parse(objAllSt);
                var arrUsuInfo = JSON.parse(objAll.infoUser);
                var arrShema = JSON.parse(objAll.schemaUser);

                objAll.infoUser = [];
                objAll.schemaUser = [];

                usuFront = jsonCrt.deserializeObject(objAll, Usuario);
                usuFront.info = arrUsuInfo;
                usuFront.shema = arrShema;

                //Get divice Info                
                this.deviceInfo = this.deviceService.getDeviceInfo();

                var usuRsp = new ResponseUsu();
                usuRsp.Data.Client.ClientToken = data["access_token"].toString();
                usuRsp.Data.Client.ClientTokenPlatform = this.deviceInfo.os.toString();
                usuRsp.Data.Client.ClientId = arrUsuInfo.idUsuario.toString();
                
                usuRsp.Auth.CredentialPlatform = this.deviceInfo.browser.toString();
                usuRsp.Auth.CredentialLogin = usuFront.name.toString();
                usuRsp.Auth.Timestamp = moment(new Date()).format().toString();
                usuRsp.Auth.CredentialPwd = "";
                usuRsp.Auth.idLoginCli = 26; //[aqui va el numero del id del proveedor/agenteGO -IPR]

                //SessionStorage Add
                this.session.set("TokenUsuFia", JSON.stringify(usuRsp).toString());
                this.session.set("nameUsuFia", usuRsp.Auth.CredentialLogin);
                this.session.set("usuIdFia", usuRsp.Auth.idLoginCli);
                
                var tokenUsu = JSON.parse(sessionStorage.getItem('TokenUsuFia')); 
                var prueba = JSON.parse(tokenUsu._value);

                setTimeout(() => {
                  /** spinner ends after 5 seconds */
                  this.spinner.hide();
                }, 2000);

                this.router.navigate(['solicitudes']);
                
            
            }else{
                this.session.clear();
                //this.router.navigate(['login']);
                this.noFoundUsu = true;
                setTimeout(() => {
                  /** spinner ends after 5 seconds */
                  this.spinner.hide();
                }, 500);
            } 
        },
        error => {
                    this.onIsError()
                    //this.router.navigate(['login']);
                    this.noFoundUsu = true;
                    setTimeout(() => {
                      /** spinner ends after 5 seconds */
                      this.spinner.hide();
                    }, 500);
                }
        );
    } else {
      this.onIsError();
      //this.router.navigate(['login']);
      this.noFoundUsu = true;
      setTimeout(() => {
        /** spinner ends after 5 seconds */
        this.spinner.hide();
      }, 500);
    }
  }


  onIsError(): void {
    this.isError = true;
    setTimeout(() => {
      this.isError = false;
      this.spinner.hide();
    }, 500);
  }



}
