import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login/login.component';
import { SolicitudesComponent } from './SolicitudesFianzas/solicitudes/solicitudes.component';
import { SolicitudesListComponent } from './SolicitudesFianzas/solicitudesComp/solicitudes-list/solicitudes-list.component';
import { SolicitudNuevaComponent } from './SolicitudesFianzas/solicitud-nueva/solicitud-nueva.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path:'solicitudes', component: SolicitudesComponent},
    {path:"solicitudes/:id", component:SolicitudesComponent},
    {path:"nueva", component:SolicitudNuevaComponent},
    {path:'', component: LoginComponent},
];

//export const routing = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
