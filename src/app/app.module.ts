import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Modules Propios
import { LoginModule} from './login/login.module';
import { DashboardModule} from './dashboard/dashboard.module';
import { SolicitudesFianzasModule} from './SolicitudesFianzas/solicitudes-fianzas.module';

//ngx-device-detector
import { DeviceDetectorModule } from 'ngx-device-detector';
//angular-webstorage-service
import { AngularWebStorageModule } from 'angular-web-storage';
//NgxSpinnerModule
import { NgxSpinnerModule } from 'ngx-spinner';

//Componentes Propios




@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoginModule,
    DashboardModule,
    SolicitudesFianzasModule,
    DeviceDetectorModule.forRoot(),
    AngularWebStorageModule,
    NgxSpinnerModule,
   

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
