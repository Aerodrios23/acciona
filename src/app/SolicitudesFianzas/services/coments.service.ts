import { Injectable } from '@angular/core';

//Import variables globales
import { rootGlobals } from 'src/app/rootGlobals';

//IPR Peticiones Rest
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Response, RequestMethod } from '@angular/http';

import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

//Models
import {ResponseSol} from '../../Models/responseSol'
import { PostRegisterSolNueva, PostRegisterSolAct } from 'src/app/Models/PostRegisterSolNueva';
import { solicitudAccionaFianzasDTO } from 'src/app/Models/solicitudes';



@Injectable({
  providedIn: 'root'
})
export class ComentsService {

  usuSol: any;

  //private endpoint: string = "http://localhost:60961/api/";

  constructor(private http: HttpClient,
              private varGlob: rootGlobals,
              ) { 
                var tokenUsu = JSON.parse(sessionStorage.getItem('TokenUsuFia')); 
                this.usuSol = JSON.parse(tokenUsu._value);
              }

  

  getAllSolicitudes(paramSol: number): Observable<any> {

    //Anexamos parametro
    this.usuSol.Data.Parameters = paramSol;
    var strJson = JSON.stringify(this.usuSol);

    //let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/ObtSolicitudesFianzas";
    let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/GetSolicitudesAcc";
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

  return this.http
  .post<solicitudAccionaFianzasDTO>(
    urlStPoint, 
    strJson,
    
    //{ headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'), }
    { headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
      //,'x-header': 'x-value'
    })}
    
  )
  .pipe(map(data => data
    ));

  }

  getNuevaSolicitud(): Observable<any> {

    //Anexamos parametro
    var strJson = JSON.stringify(this.usuSol);

    let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/ObtSolicitudNueva";
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    return this.http
      .post<ResponseSol>(
        urlStPoint, 
        strJson,
        { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'), }
      )
      .pipe(map(data => data
        ));
  
  }


  postNuevaSolicitud(dataForm: PostRegisterSolNueva): Observable<any> {

   
    let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/upFilesSolNew";
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
   

    return this.http
      .post<any>(
      urlStPoint, 
      dataForm,
      { headers: new HttpHeaders().set('Content-Type', 'application/json'), }
    )
    .pipe(map(data => data
      ));


  }


  getFilesSolicitud(numSol: number): Observable<any> {

    //Anexamos parametro
    this.usuSol.Data.Parameters = numSol;
    var strJson = JSON.stringify(this.usuSol);

    //let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/GetFilesSolAcc?numSol=20";
    let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/GetFilesSolAcc";
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    let body = new HttpParams();
    body = body.set('numSol','20');

    return this.http.post(urlStPoint, numSol).pipe(
      map(data => data));

  }


  getFile(numFile: number): Observable<any> {

    //Anexamos parametro
    this.usuSol.Data.Parameters = numFile;
    var strJson = JSON.stringify(this.usuSol);

    let urlStPoint2 = "Solicitudes/GetFile?numFile="+ numFile.toString();
    //let urlStPoint2 = "Solicitudes/GetFile?numFile="+ numFile.toString() + "&numDoc=" + numDoc;
    let urlStPoint: string = this.varGlob.endpoint + urlStPoint2;
   
    return this.http.post(urlStPoint, RequestMethod.Post, { headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
      ,'accept': 'application/json'
      ,'Authorization':'Bearer CE-807dit1pU6ScAAvTFdK6ftGb8hVHRGw5lWES0JIhm8yEpnFirvyWiOKl69BcK_OcTqQF9tHrHqUEueWPP_SUhqcskWpdNbo0IndkigyA-H8wk1jBtBYtum-4bA6rLAscMg2IcPbJpHuxvqIRLOLCMLio8Ods_BlFrmd532Zg9VhRnDgtlXiqtEAMs9wkCgVBW0k0VYVpi4LGtWnF7j1C_ZeZizXRe5t8yvfeI4EbFkr_SiwyOXN6ZAOBQIJ9RWpGVRRdh8GzFUwQEsqdX-6VtD2NJXz3gus9etjg8Q3wGr4DJo9itvBEtEDFhagDHya35K3bZItXUREBPGHRJiQ6IPWiyLL6nYqxNrEpdE5EjSen395IKYxevCl3l8Vt_7qm-b4v5EfX4YqcEHzpvQs0qBngOkuAtb4hJuRdUHw0QGfSDcUj1eSUUZgpeN1CMdU7z6AsczXiqiT4uTVMgUWrfUuYmRVBxRlI21gVqhoHTIQ8RVKp7dUbjDNP7NBGVKHJdTerRTVLx7AojWujfdWtDDatNXS4YCjTnTiTUTaLr080kuv-E5Rt6GGJo_w1AH0JyoWekd8r_D1LpBVdd2dH7TVsCQaa_tIIMU0xdQydvbeqCUWTrp2OPV1SXNF42WAgsCmMQ2CAdaaGRbrnWg5NbdQx_meZ_nmn8sBRsJlV-nCl_W2DUxggtdAJrmC-NtsT26HoAThqb8Qy3Tz3MWPgmXjOfyS_RUVXdlGYfoTiKV5oduhxOCxwJ1mUA-WSs7lD6Dg2GEOg-s1TVacF5dmV1nlscE9ZGAIhZWBJbLEFcFO31YPHRUoG-Cgn0S6E_CW7lF7fT9hXEfYcNERZhUA'
    }),
    responseType:'blob'
  })
    .map((x) => {
        return x;
    });

  }

  postUpdateSolicitud(dataForm: PostRegisterSolAct): Observable<any> {

   
    let urlStPoint: string = this.varGlob.endpoint + "Solicitudes/UpdateFia";
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null
   

    return this.http
      .post<any>(
      urlStPoint, 
      dataForm,
      { headers: new HttpHeaders().set('Content-Type', 'application/json'), }
    )
    .pipe(map(data => data
      ));


  }


}
