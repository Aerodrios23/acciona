import { TestBed, inject } from '@angular/core/testing';

import { MessageServiceSolService } from './message-service-sol.service';

describe('MessageServiceSolService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MessageServiceSolService]
    });
  });

  it('should be created', inject([MessageServiceSolService], (service: MessageServiceSolService) => {
    expect(service).toBeTruthy();
  }));
});
