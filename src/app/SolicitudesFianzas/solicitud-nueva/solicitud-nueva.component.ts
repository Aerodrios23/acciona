import { Component, OnInit, Renderer2, ElementRef, ViewChild, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { trigger, state, style, animate, transition} from '@angular/animations';
import { HttpClient, HttpResponse, HttpRequest, 
  HttpEventType, HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { catchError, last, map, tap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';

import { Router } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { of } from 'rxjs/observable/of';

import * as moment from 'moment';
import {JsonConvert, JsonConverter, OperationMode, ValueCheckingMode} from "json2typescript"
import Swal from 'sweetalert2'

//MODELS
import { solicitudNuevaDTO, solicitudNuevaDTONew } from 'src/app/Models/solicitudes';
import { TipoSolicitud, TipoSolicitudNew } from 'src/app/Models/tipoSolicitud';
import { TipoDocumento } from 'src/app/Models/tipoDocu';
import { FileUploadModel } from 'src/app/Models/fileData';
import { fileData } from 'src/app/Models/fileNewSol';
import { FilesSolNuevaDTO } from 'src/app/Models/filesSolNuevaDTO';
import { PostRegisterSolNueva } from 'src/app/Models/PostRegisterSolNueva';


//SERVICES
import { ComentsService } from '../services/coments.service';
import { TipoSubRamo } from 'src/app/Models/fiaCore';
import { TipoMovimiento } from 'src/app/Models/tipoMov';





declare var $;

@Component({
  selector: 'app-solicitud-nueva',
  templateUrl: './solicitud-nueva.component.html',
  styleUrls: ['./solicitud-nueva.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ]
})
export class SolicitudNuevaComponent implements OnInit {

  @ViewChild("hDocs") hDocs: ElementRef;

  altSolForm: FormGroup;
  //solNew: solicitudNuevaDTO = new solicitudNuevaDTO();
  solNew: solicitudNuevaDTONew = new solicitudNuevaDTONew();
  usuSol: any;
  dateNow: any;
  
  divDocs; any;
  //arrTipSols: TipoSolicitud []; 
  arrTipSols: TipoSolicitudNew []; 
  arrSubRamos: TipoSubRamo []; 
  arrTipSol: TipoSolicitudNew[];
  arrTipDocs: Array<TipoDocumento> = [];
  //arrFileDocs: fileData[] = [];
  arrFileDocs: Array<FilesSolNuevaDTO> = [];
  postRegNewSol: PostRegisterSolNueva = new PostRegisterSolNueva();



  fileUpload: HTMLInputElement;
  value = 50;

//FileUpload
/*
//***************************************************************************************************************** /**         Link text */
      @Input() text = 'Upload';
      /** Name used in form which will be sent in HTTP request. */
      @Input() param = 'file';
      /** Target URL for file uploading. */
      @Input() target = 'https://file.io';
      /** File extension that accepted, same as 'accept' of <input type="file" />. 
          By the default, it's set to 'image/*'. */
      @Input() accept = 'image/*, .pdf, .doc, .xls, .xlsx, .docx, ppt, pptx, .xml';
      /** Allow you to add handler after its completion. Bubble up response text from remote.**/
      @Output() complete = new EventEmitter<string>();

     // private filesUpMod: Array<FileUploadModel> = [];

      formData = new FormData(); 

      stMatPro: string = "void";

   
      private filesLoad: any[] = [];
      private fileReader = new FileReader();


//******************************************************************************************************************/
  constructor(
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private commentService: ComentsService,
    public renderer: Renderer2,
    private _http: HttpClient,
    private router: Router,
    public session: SessionStorageService
  ) { 
    
    var tokenUsu = JSON.parse(sessionStorage.getItem('TokenUsuFia')); 
    this.usuSol = JSON.parse(tokenUsu._value);

    moment.locale('es');

    setInterval(() => {
      this.dateNow = moment().format('LLLL');
    }, 1);
  

  }

  ngOnInit() {
    //Validacion de los campos del formulario
    this.altSolForm = this.formBuilder.group({
      slRamo: ['', Validators.compose([Validators.required]) ],
      slSubRamo: ['', Validators.compose([Validators.required]) ],
      slTipFia: ['', Validators.compose([Validators.required]) ],
      slBenf:   ['', Validators.compose([Validators.required])],
      slTipMov: ['', Validators.compose([Validators.required])],
      slAfin:   ['', Validators.compose([Validators.required])],
      slGere:   ['', Validators.compose([Validators.required])],
      txtaDetSol: ['', Validators.compose([Validators.required])],
      done: false
    });


    $(document).ready(() => {
      
      const trees: any = $('[data-widget="tree"]');
      trees.tree();

      $('.ui:not(.container, .grid)').each(function() {
      $(this)
        .popup({
          on        : 'hover',
          variation : 'small inverted',
          exclusive : true,
          content   : $(this).attr('class')
        })
      ;
      });



      

    });

    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
  
    /***********************************************************************************************************/
    this.getSolicitudNueva();

    /***********************************************************************************************************/
    
    document.querySelector("html").classList.add('js');



  }

  ngOnChanges(){
    
  
  }

  ngOnDestroy(): void {
    document.body.className = '';
  }

  //------------------------------ Metodos ----------------------------------------------
  getSolicitudNueva() {

    let jsonCrt = new JsonConvert();
    this.spinner.show();    
    
    this.commentService.getNuevaSolicitud().subscribe(
      data => {
        var prueba = data;
     
        var arrGerens = JSON.stringify(data.solicitudNuevaDTO.lstGerencias);
        var arrRamos = JSON.stringify(data.solicitudNuevaDTO.lstTipRamos);
        var arrSubRamos = JSON.stringify(data.solicitudNuevaDTO.lstTipSubRamos);
        var arrTipSOl =  JSON.stringify(data.solicitudNuevaDTO.lstTipSolicitudes);
        var arrTipMov =  JSON.stringify(data.solicitudNuevaDTO.lstTipMovimientos);
        var arrAfianz =  JSON.stringify(data.solicitudNuevaDTO.lstAfianzadoras);
        var arrBenef =  JSON.stringify(data.solicitudNuevaDTO.lstBeneficiarios);
       

        this.solNew.lstGerencias = JSON.parse(arrGerens);
        this.solNew.lstTipRamos = JSON.parse(arrRamos);
        this.solNew.lstTipSubRamos = JSON.parse(arrSubRamos);

        this.solNew.lstTipMovimiento = JSON.parse(arrTipMov);
        this.solNew.lstTipSol = JSON.parse(arrTipSOl);
        this.solNew.lstAfianzadoras = JSON.parse(arrAfianz);
        this.solNew.lstBeneficiarios = JSON.parse(arrBenef);

        this.spinner.hide();
      }
    );
   
  }

 
//Ok Form Alta Solicitud
//*************************************************************************************************************/

  onAltaSol(){

    if (this.altSolForm.valid ) {

      Swal.fire({
        title: 'Generar una Solictud',
        text: "¿Estas seguro de generar una solictud?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, estoy de acuerdo!'
      }).then((result) => {
    
        this.spinner.show();  

        if ( this.arrFileDocs.length > 0  ) {
    
          this.postRegNewSol.userName = this.session.get("nameUsuFia");
          this.postRegNewSol.userId = 1;//this.session.get("usuIdFia"); //Debe tomar forzamente los usuarios del core de fianzas
          this.postRegNewSol.userDate =  moment().format();

          this.postRegNewSol.ramoId = this.altSolForm.get('slRamo').value;
          this.postRegNewSol.subRamoId = this.altSolForm.get('slSubRamo').value;
          this.postRegNewSol.paqId = this.altSolForm.get('slTipFia').value;
          
          this.postRegNewSol.tipMoviId = this.altSolForm.get('slTipMov').value;
          this.postRegNewSol.clieId = 1;                                    //[dato que mandara Coria en la respuesta del login] debe tomar el cliente del Core de fianzas
          
          this.postRegNewSol.gereId = this.altSolForm.get('slGere').value;
          this.postRegNewSol.afianId = this.altSolForm.get('slAfin').value;

          this.postRegNewSol.benfId = 16; //this.altSolForm.get('slBenf').value; //Debe tomar forzamente los usuarios del core de fianzas
          this.postRegNewSol.nameBenef = this.solNew.lstBeneficiarios.filter(x => x.CVE_BENEF == this.altSolForm.get('slBenf').value)[0].RAZON_SOCIAL_BENEF.toString();
          
          this.postRegNewSol.detSol = this.altSolForm.get('txtaDetSol').value;
          this.postRegNewSol.filesSolNuevaDTO = this.arrFileDocs;

          this.commentService.postNuevaSolicitud(this.postRegNewSol).subscribe(
                data => {
                  var prueba = data;
    
                  if(data.codeResult == 0){

                    this.spinner.hide();  
                    
                    Swal.fire(
                      'Gurdado!',
                      '¡Solicitud: ' + data.solicitudInsDTO.NUM_SOL + ' gurdada con exito!',
                      'success'
                    )
                    
                    this.router.navigate(["/solicitudes", 0]).then( (e) => {
                    });

                  }else{
                    this.spinner.hide();  
                    Swal.fire(
                      'Error!',
                      '¡Contactar a Sistemas Ordás-Howden!',
                      'error'
                    )
                  }
                }
              );

        }else{
          this.spinner.hide();  
          Swal.fire(
            'Error!',
            '¡Debes cargar al menos 1 documento!',
            'error'
          )
          
        }
      
      });
   
    }

  }

//FileUploas Methods
//*************************************************************************************************************/
  
  onClick(id) {
      this.stMatPro = "in";
      var tipDoc =  this.arrTipDocs.filter(
                    doc => doc.CVE_TIPO_DOC === id);
        this.fileUpload = document.getElementById(id) as HTMLInputElement;
        this.fileUpload.click();
        this.stMatPro = "void";
  }


  onChange(event: Event, id) {
    let evFiles = event.target['files'];

    var tipDoc =  this.arrTipDocs.filter(doc => doc.CVE_TIPO_DOC === id);
    this.fileUpload = document.getElementById(id) as HTMLInputElement;
    document.getElementById(tipDoc[0].idTipDoc.toString()).innerHTML = evFiles[0].name;

    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, id);
    }
  };


  private readFiles(filesLoad: FileDataForm, index: number, idDoc: number) {
    
    let file = filesLoad[index];
    let fileNewSol: FilesSolNuevaDTO = new FilesSolNuevaDTO();
   
    fileNewSol.nameFile = file.name.split('.')[0] + "." + file.name.split('.').pop();
    fileNewSol.sizeFile = this.formatBytes(file.size, 0).toString();
    fileNewSol.extFile = file.name.split('.').pop();
    fileNewSol.idTipFile = idDoc;
    fileNewSol.typeFile = file.type.toString();


    this.fileReader.onload = () => {
      fileNewSol.dataFile = this.fileReader.result.toString();
      this.arrFileDocs.push(fileNewSol);
      this.formData.append(fileNewSol.nameFile.toString(), file);
    };
    this.fileReader.readAsDataURL(file);
  }

  formatBytes(bytes, decimals) {
    if (bytes == 0) return 0;
    var k = 1024;
    var dm = decimals + 1 || 2;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }


 //Coments
/************************************************************************************************************/
 
getSubRamoMethod(ev) {
    
  this.arrSubRamos = this.solNew.lstTipSubRamos;
  let idSubRamo = ev.source.value;
  
  this.arrSubRamos =  this.arrSubRamos.filter( tipSubRamo => tipSubRamo.CVE_RAMO === idSubRamo);
    
}

getTipFianza(ev) {
  
  this.arrTipSol = this.solNew.lstTipSol;
  let idTipMov = ev.source.value;
  //Añadir CVE de la afianzadora
  //this.arrTipSol =  this.arrTipSol.filter( tipMov => tipMov.cveTipDoc === idTipMov);
}

showFilesFia(ev){
   
    this.arrTipSols = this.solNew.lstTipSol;
    let idTipPaq = ev.source.value;
    var selectedIds =  this.arrTipSols.filter(tipSol => tipSol.CVE_PAQUETE === idTipPaq);
    this.arrTipDocs = selectedIds[0].lstTipDocumentos;
    
    var prueba = this.arrTipDocs[0].DES_DOCUMENTO.toString().replace(/\s/g, ""); 

    this.arrTipDocs.forEach( obj => {
      obj.idTipDoc = obj.DES_DOCUMENTO.toString().replace(/\s/g, ""); 
    
    })
  
}


}




