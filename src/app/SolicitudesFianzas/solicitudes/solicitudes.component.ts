import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { DeviceDetectorService } from 'ngx-device-detector';
import { ActivatedRoute } from '@angular/router';

//Services
import { ComentsService } from '../services/coments.service';

declare var $;

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {

  statusSol: number = 0;
  
  constructor(
    private commentService: ComentsService,
    private deviceService: DeviceDetectorService,
    private route: ActivatedRoute,
    private cd:ChangeDetectorRef
  ) { }

  ngOnInit() {

    $(document).ready(() => {
      
      const trees: any = $('[data-widget="tree"]');
      trees.tree();

      /*
      this.dataTable = $(this.table.nativeElement);
      this.dataTable.dataTable({
        responsive: true
      });
      */

     $('.ui:not(.container, .grid)').each(function() {
      $(this)
        .popup({
          on        : 'hover',
          variation : 'small inverted',
          exclusive : true,
          content   : $(this).attr('class')
        })
      ;
    });


    });


    window.dispatchEvent(new Event('resize'));
    document.body.className = 'hold-transition skin-blue sidebar-mini';
    
    //Get params the other component
    //this.statusSol = Number.isNaN(parseInt(this.route.snapshot.paramMap.get('id'))) ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
    this.route.paramMap.subscribe(params => { 
      this.statusSol = Number.isNaN(parseInt(this.route.snapshot.paramMap.get('id'))) ? 0 : parseInt(this.route.snapshot.paramMap.get('id'));
      //this.cd.detectChanges();
      this.cd.markForCheck();
    })

  }

  ngOnDestroy(): void {
    document.body.className = '';
  }
}
