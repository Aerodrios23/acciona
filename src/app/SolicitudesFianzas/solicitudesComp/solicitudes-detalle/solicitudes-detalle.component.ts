import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { Router } from '@angular/router';

//Material
import {MAT_DIALOG_DATA, MatDialogRef, DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS} from '@angular/material';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

import { NgxSpinnerService } from 'ngx-spinner';

//Forms
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

//animations
import { trigger, state, style, animate, transition} from '@angular/animations';

import * as moment from 'moment';

import Swal from 'sweetalert2'

//Models
import { solicitudFianzasDTO } from 'src/app/Models/solicitudes';
import { PostRegisterSolAct } from 'src/app/Models/PostRegisterSolNueva';
import { TipoDocumento } from 'src/app/Models/tipoDocu';
import { FilesSolNuevaDTO } from 'src/app/Models/filesSolNuevaDTO';


import * as FileSaver from 'file-saver';

import { ComentsService } from '../../services/coments.service';
import { from } from 'rxjs';






export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};




@Component({
  selector: 'app-solicitudes-detalle',
  templateUrl: './solicitudes-detalle.component.html',
  styleUrls: ['./solicitudes-detalle.component.scss'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: 'green',
        width: '100px',
        height: '100px'
      })),
      state('final', style({
        backgroundColor: 'red',
        width: '200px',
        height: '200px'
      })),
      transition('initial=>final', animate('1500ms')),
      transition('final=>initial', animate('1000ms'))
    ]),

    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: 'green',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: 'red',
        transform: 'scale(1.5)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1500ms'))
    ]),

    trigger('fadeInOut', [
      state('void', style({
        opacity: 0
      })),
      transition('void <=> *', animate(1000)),
    ]),

    trigger('EnterLeave', [
      state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.5s 300ms ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('fadeInOut', [
      state('in', style({ opacity: 100 })),
      transition('* => void', [
            animate(300, style({ opacity: 0 }))
      ])
    ])
  ],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter},
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
  
})

export class SolicitudesDetalleComponent implements OnInit {

  //FileUpload
/*
//******************  Upload File   ********************************************* /**        
@Input() text = 'Upload';
/** Name used in form which will be sent in HTTP request. */
@Input() param = 'file';
/** Target URL for file uploading. */
@Input() target = 'https://file.io';
/** File extension that accepted, same as 'accept' of <input type="file" />. 
    By the default, it's set to 'image/*'. */
@Input() accept = 'image/*, .pdf, .doc, .xls, .xlsx, .docx, ppt, pptx, .xml';
/** Allow you to add handler after its completion. Bubble up response text from remote.**/
@Output() complete = new EventEmitter<string>();

stMatPro: string = "void";
arrTipDocs: Array<TipoDocumento> = [];
fileUpload: HTMLInputElement;
arrFileDocs: Array<FilesSolNuevaDTO> = [];

private filesLoad: any[] = [];
private fileReader = new FileReader();

//******************************************************************************* /**    


  @Input() solDetFianza: solicitudFianzasDTO;
  @Output() saveSolFianzaEvent = new EventEmitter<solicitudFianzasDTO>();
  @Output() closeSolFianzaEvent = new EventEmitter();


  show: boolean;
  btnDis: boolean;
  numSolucion: number;
  numAfianzadora: number;
  numDocument: number = 0;
  display: boolean;
  solfiaPrueba : solicitudFianzasDTO;
  postRegUpdSol: PostRegisterSolAct = new PostRegisterSolAct();
  

  //detSolForm: FormGroup;
  fianzaForm: FormGroup;
  contracForm: FormGroup;
  reciboForm: FormGroup;

  date = new FormControl(moment().locale('es'));

  constructor(
    private formBuilder: FormBuilder,
    private commentService: ComentsService,
    public dialogRef: MatDialogRef<SolicitudesDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private router: Router,
    private spinner: NgxSpinnerService,
  ) { }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

 

  ngOnInit() {

    //moment.locale('es');
    
    this.display = true;

    var prueba = this.data.solDetFianza;
    this.solfiaPrueba = this.data.solDetFianza;
    this.numSolucion = this.data.solDetFianza.NUM_SOL;
    this.numAfianzadora = this.data.solDetFianza.CVE_AFI;
   
    if(this.data.solDetFianza.lstFilesFia){
      this.show = this.data.solDetFianza.lstFilesFia.length == 0 ? true : false;
     
    }else{
      this.show = true;
    }
    
    if(this.data.solDetFianza.lstDocsFia){
      this.btnDis = this.data.solDetFianza.lstDocsFia.length == 0 ? true : false;
    }else{
      this.btnDis= true;
    }

    this.contracForm = this.formBuilder.group({
      contrAfi:   ['', Validators.compose([Validators.required])],
      dateContr:   ['', Validators.compose([Validators.required])],
      dateCumContr:   ['', Validators.compose([Validators.required])],
      tipContAfi:   ['', Validators.compose([Validators.required])],
      gerContr:   ['', Validators.compose([Validators.required])],
    });

    
    this.fianzaForm = this.formBuilder.group({
      
      numFia:   ['', Validators.compose([Validators.required])],
      vigFia:   ['', Validators.compose([Validators.required])],
      monFia:   ['', Validators.compose([Validators.required])],
      desAfi:   [],
      descSol:   [],
      benFia:   [],
      
      /*
      folRec:   ['', Validators.compose([Validators.required])],
     
      
      primFia:  ['', Validators.compose([Validators.required])],
      comRec:   ['', Validators.compose([Validators.required])],
      uuidRec:   ['', Validators.compose([Validators.required])],
      emiRec:   ['', Validators.compose([Validators.required])],
      */
    });
  
    this.reciboForm = this.formBuilder.group({
      folRec:   ['', Validators.compose([Validators.required])],
      relatA:   ['', Validators.compose([Validators.required])],
      comRec:   ['', Validators.compose([Validators.required])],
      primFia:   ['', Validators.compose([Validators.required])],
      uuidRec:   ['', Validators.compose([Validators.required])],
    });


  }


  ngOnChanges(changes:any){
   
   

  }


  closeFormDialog() {
    this.display = false;
    this.closeSolFianzaEvent.emit();
  }

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
    this.formControl.hasError('email') ? 'Not a valid email':'';

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    //this.dataService.updateIssue(this.data);
  }

  DownLoadFiles(file, numDoc){
   
    this.spinner.show();

    this.commentService.getFile(file.ID_DOCUMENTO).subscribe(
      data => {
        var prueba = data;

        var ext = this.getTypeFile(file.NOM_DOCUMENTO);
        var blob = new Blob([data], {type: ext} );
        this.spinner.hide();
        
        return FileSaver.saveAs(blob, file.NOM_DOCUMENTO);

    });
 
  }
    

  getTypeFile(extFile) {
    
    var fileType = 'application/octet-stream';
    var fileExtension = extFile.split('.').pop();

    if (fileExtension == 'jpg') {
        fileType = 'image/jpg';
    }
    else if (fileExtension == 'png') {
        fileType = 'image/png';
    }else if (fileExtension == 'gif') {
        fileType = 'image/gif';
    }else if (fileExtension == 'tiff') {
        fileType = 'image/tiff';
    }else if (fileExtension == 'jpeg' || fileExtension == 'jpg') {
        fileType = 'image/jpeg';
    }else if (fileExtension == 'pdf') {
      fileType = 'application/pdf';
    }else if (fileExtension == 'txt') {
      fileType = 'text/plain';
    }else if (fileExtension == 'doc' || fileExtension == 'docx' ) {
      fileType = 'application/vnd.ms-word';
    }else if (fileExtension == 'xls ' || fileExtension == 'xlsx') {
      fileType = 'application/vnd.ms-excel';
    }else if (fileExtension == 'csv') {
      fileType = 'text/csv';
    }
    
    return fileType;

  }

  //******************* FILE UPLOAD ******************************************/

  onClickFile(id, numDoc) {
    this.stMatPro = "in";
    this.numDocument = numDoc;
    var tipDoc =  this.arrTipDocs.filter(
                  doc => doc.CVE_TIPO_DOC === id);
      this.fileUpload = document.getElementById(id) as HTMLInputElement;
      this.fileUpload.click();
      this.stMatPro = "void";
  }

  onChange(event: Event, id) {
    
    let evFiles = event.target['files'];
  
    //var tipDoc =  this.arrTipDocs.filter(doc => doc.CVE_TIPO_DOC === id);
    this.fileUpload = document.getElementById(id) as HTMLInputElement;
    
    switch(id){
      case 3:
        document.getElementById("spFianza").innerHTML = evFiles[0].name;
        break;

      case 7:
        document.getElementById("spRecibo").innerHTML = evFiles[0].name;
        break;

      case 8:
        document.getElementById("spXML").innerHTML = evFiles[0].name;
        break;

    }
   
    


    if (event.target['files']) {
      this.readFiles(event.target['files'], 0, id);
    }
  
  };

  private readFiles(filesLoad: FileDataForm, index: number, idDoc: number) {
    
    let file = filesLoad[index];
    let fileFiaSol: FilesSolNuevaDTO = new FilesSolNuevaDTO();
   
    fileFiaSol.nameFile = file.name;
    fileFiaSol.numDoc = this.numDocument;
    fileFiaSol.sizeFile = this.formatBytes(file.size, 0).toString();
    fileFiaSol.extFile = file.name.split('.').pop();
    fileFiaSol.idTipFile = idDoc;
    fileFiaSol.typeFile = file.type.toString();


    this.fileReader.onload = () => {
      fileFiaSol.dataFile = this.fileReader.result.toString();
      console.log(fileFiaSol.dataFile);
      this.arrFileDocs.push(fileFiaSol);
      //this.formData.append(fileNewSol.nameFile.toString(), file);
    };
    this.fileReader.readAsDataURL(file);
  }

  formatBytes(bytes, decimals) {
    if (bytes == 0) return 0;
    var k = 1024;
    var dm = decimals + 1 || 2;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }


  onActFia(){

  }

  //******************* SAVE EMISION ******************************************/

  onSaveFia(){

    if (this.fianzaForm.valid ) {

      Swal.fire({
        title: 'Actualizar la Solictud ' + this.numSolucion,
        text: "¿Estas seguro de actualiar esta solictud?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'SI, estoy de acuerdo!'
      }).then((result) => {
    
        
          
          if(!result.dismiss){

            this.spinner.show();

            /*
            this.postRegUpdSol.numSol = this.numSolucion;
            this.postRegUpdSol.numFia = this.emiSolForm.get('numFia').value;
            this.postRegUpdSol.idAfi =  this.numAfianzadora;
            this.postRegUpdSol.dateVig = this.emiSolForm.get('vigFia').value;
            this.postRegUpdSol.montoRec = this.emiSolForm.get('monFia').value;
            this.postRegUpdSol.folioRec = this.emiSolForm.get('folRec').value;
            this.postRegUpdSol.comRec = this.emiSolForm.get('comRec').value;
            this.postRegUpdSol.primaRec = this.emiSolForm.get('primFia').value;
            this.postRegUpdSol.relatFia = this.emiSolForm.get('relatA').value;
            this.postRegUpdSol.dateRec = this.emiSolForm.get('emiRec').value;
            this.postRegUpdSol.uuidRec = this.emiSolForm.get('uuidRec').value;
            */
       
            this.postRegUpdSol.filesSolNuevaDTO = this.arrFileDocs;
            
            console.log(this.postRegUpdSol.filesSolNuevaDTO);
  
            this.commentService.postUpdateSolicitud(this.postRegUpdSol).subscribe(
                  data => {
                    var prueba = data;
      
                    if(data.codeResult == 0){
  
                      this.spinner.hide();
                      
                      Swal.fire({
                        type: 'success',
                        title: 'Actualizado!',
                        html: '¡Solicitud: <strong>' + this.numSolucion + '</strong> actualizada con exito!',
                        timer: 2000,
                        onBeforeOpen: () => {
                         
                        },
                        onClose: () => {
                          this.dialogRef.close();
                          this.router.navigate(["/solicitudes", 0]).then( (e) => {
                          });
                        }
                      });
                      
                     
  
                    }else{
                      Swal.fire(
                        'Error!',
                        '¡Contactar a Sistemas Ordás-Howden!',
                        'error'
                      )
                      
                      this.dialogRef.close();

                      this.router.navigate(["/solicitudes", 0]).then( (e) => {
                      });
                    
                    }
  
  
                  }
                );
  
              

          }
     
      });
   
    }

  }


  }


 





