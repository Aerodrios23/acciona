import { Component, OnInit, ViewChild, Input, SimpleChanges, ChangeDetectionStrategy } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog} from '@angular/material';
import { ActivatedRoute } from '@angular/router';

//Tools
import { ConfirmationService } from 'primeng/primeng';
import { NgxSpinnerService } from 'ngx-spinner';

//Servicios
import { ComentsService } from 'src/app/SolicitudesFianzas/services/coments.service';
import { SecurityService } from 'src/app/Servicios/security.service';

//Models
import { solicitudFianzasDTO, solicitudAccionaFianzasDTO } from 'src/app/Models/solicitudes';

//Components
import { SolicitudesDetalleComponent } from '../solicitudes-detalle/solicitudes-detalle.component';
import { fileFianza } from 'src/app/Models/fileNewSol';
import { DomSanitizer } from '@angular/platform-browser';



@Component({
  selector: 'app-solicitudes-list',
  templateUrl: './solicitudes-list.component.html',
  styleUrls: ['./solicitudes-list.component.scss'],
  // your selector and template
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class SolicitudesListComponent implements OnInit {
  
  solicitudesLst: solicitudAccionaFianzasDTO[];
  solDetFianza: solicitudAccionaFianzasDTO;
  fileDocsLst: Array<fileFianza> = []; 
  fileFianzaLst: Array<fileFianza> = []; 
  show:boolean = false;
  
  displayedColumns: string[] = ['NUM_SOL', 'DES_TIPO_SOL', 'DES_USUARIO', 'FEC_SOLICITUD', 'DES_STATUS_SOL', 'actionsColumn'];
  dataSourseSol = new MatTableDataSource();

  @Input() childStatusSol: string;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private commentService: ComentsService,
    private securityService: SecurityService,
    private confirmationService: ConfirmationService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private sanitizer: DomSanitizer,
  ) { }


  //Carga Inicial
  ngOnInit() {
    this.dataSourseSol.paginator = this.paginator;
    this.dataSourseSol.sort = this.sort;


    this.getAllSolicitudes2(this.childStatusSol);

  }
  
 
 //Metodos
  getAllSolicitudes2(statusSol) {

    this.spinner.show();    

    this.commentService.getAllSolicitudes(statusSol).subscribe(
      data => {
        var prueba = data;
        console.log(prueba);
        this.solicitudesLst = data.solicitudesAccDTO;
        this.dataSourseSol.data =  data.solicitudesAccDTO;

        this.spinner.hide();
        
      }
    );

   
  }

  //*******************  Functions DataMaterial********************* */
  
  applyFilter(filterValue: string) {
    this.dataSourseSol.filter = filterValue.trim().toLowerCase();
    if (this.dataSourseSol.paginator) {
      this.dataSourseSol.paginator.firstPage();
    }
  }

  private refreshTable() {
    // Refreshing table using paginator
    this.paginator._changePageSize(this.paginator.pageSize);
  }


  //Styles for diferent status SOLICITUD    
  getStyleClass(colRow) {
    
    switch (colRow.CVE_STATUS_SOL) {
      case 1:
        return "bt-color-1";  
        //return "btn btn-app bg-maroon";
      case 2:
        return "bt-color-2";
      case 3:
        return "bt-color-3";
      case 4:
        return "bt-color-4";
      case 5:
        return "bt-color-5";
      case 6:
        return "bt-color-6";
      
        default:
        return "bt-color-1";
    }
    
  }


  startEdit(solDetFianza: solicitudAccionaFianzasDTO) {
    
    this.spinner.show();

    //Cargar los documentos de la solicitud
    this.commentService.getFilesSolicitud(solDetFianza.NUM_SOL).subscribe(
      data => {

        var prueba = data;

        this.fileDocsLst = [];
        this.fileFianzaLst = [];

        data.solDetAccDTO.FILES_SOL.forEach( (file) => {
        
          let fileNew = new fileFianza();
          fileNew.NUM_SOL = file.NUM_SOL;
          fileNew.NOM_DOCUMENTO = file.NOM_DOCUMENTO;
          fileNew.ID_TIPO = file.ID_TIPO;
          fileNew.ID_DOCUMENTO = file.ID_DOCUMENTO;
          fileNew.FS_DOCUMENTO = file.FS_DOCUMENTO;
          fileNew.DES_TIP = file.DES_TIP;
          fileNew.CONTEXTO = file.CONTEXTO;
          fileNew.b_DOCUMENTO = file.b_DOCUMENTO;
          fileNew.s_DOCUMENTO = file.s_DOCUMENTO;

          switch(file.ID_TIPO){
            case 3:
            case 7:
            case 8:
                this.fileFianzaLst.push(fileNew);
            break;
               
            default:
                this.fileDocsLst.push(fileNew);
            break;
          }
 
        });

        solDetFianza.NUM_FIA = data.solDetAccDTO.NUM_FIA;
        solDetFianza.NUM_REC_FIA = data.solDetAccDTO.NUM_REC_FIA;
        solDetFianza.CVE_AFI = data.solDetAccDTO.CVE_AFI;
        solDetFianza.FEC_VIG_FIA = data.solDetAccDTO.FEC_VIG_FIA;
        solDetFianza.UUID_REC = data.solDetAccDTO.UUID_REC;
        solDetFianza.FEC_REC = data.solDetAccDTO.FEC_REC;
        solDetFianza.MON_REC = data.solDetAccDTO.MON_REC;
        solDetFianza.COM_REC = data.solDetAccDTO.COM_REC;
        solDetFianza.PRIMA_FIA = data.solDetAccDTO.PRIMA_FIA;
        solDetFianza.VALOR_REFERENCIA1 = data.solDetAccDTO.VALOR_REFERENCIA1;

        solDetFianza.lstDocsFia = this.fileDocsLst;
        solDetFianza.lstFilesFia = this.fileFianzaLst;

        this.spinner.hide();

        /*Open Dialog*/
        const dialogRef = this.dialog.open(SolicitudesDetalleComponent, {
          data: {solDetFianza, show:this.show}
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result === 1) {
            this.refreshTable();
          }
        });
      }
    );
    
   
  }

  //****************** Detalles Solicitud ************************************/

  selectSolFianza(solDetFianza: solicitudAccionaFianzasDTO) {
    this.solDetFianza = Object.assign({}, solDetFianza);
  }

  //************* Cerrar Dialog *************************************************/
  closeDialogForm() {
    this.solDetFianza = null;
  }

  //***************************************************************************** */
  ngOnChanges(changes: SimpleChanges) {
    
    for (let propName in changes) {  
      
      let change = changes[propName];
      this.getAllSolicitudes2(change.currentValue);
    }
  }

  getTypeFile(extFile) {
    
    var fileType = 'application/octet-stream';
    var fileExtension = extFile.split('.').pop();

    if (fileExtension == 'jpg') {
        fileType = 'image/jpg';
    }
    else if (fileExtension == 'png') {
        fileType = 'image/png';
    }else if (fileExtension == 'gif') {
        fileType = 'image/gif';
    }else if (fileExtension == 'tiff') {
        fileType = 'image/tiff';
    }else if (fileExtension == 'jpeg' || fileExtension == 'jpg') {
        fileType = 'image/jpeg';
    }else if (fileExtension == 'pdf') {
      fileType = 'application/pdf';
    }else if (fileExtension == 'txt') {
      fileType = 'text/plain';
    }else if (fileExtension == 'doc' || fileExtension == 'docx' ) {
      fileType = 'application/vnd.ms-word';
    }else if (fileExtension == 'xls ' || fileExtension == 'xlsx') {
      fileType = 'application/vnd.ms-excel';
    }else if (fileExtension == 'csv') {
      fileType = 'text/csv';
    }
    
    return fileType;

  }

 

}
