import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataViewModule } from 'primeng/dataview';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

//Material Desing
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

//DATAPICKER RANGE
import { MyDateRangePickerModule } from 'mydaterangepicker';
//PrimeNg
import { PanelModule, DataTableModule, SharedModule, ButtonModule, DialogModule, InputTextModule,
        CalendarModule, DropdownModule, ConfirmDialogModule, ConfirmationService, OrderListModule,
        StepsModule
} from 'primeng/primeng';

import {TableModule } from 'primeng/table';

import { MatTableModule, MatFormFieldModule, MatInputModule, MatPaginatorModule,
         MatButtonModule, MatIconModule, MatDialogModule, MatTabsModule,
         MatCardModule, MatSlideToggleModule, MatSelectModule, MatProgressBarModule, MatDatepickerModule,
         MatNativeDateModule } from '@angular/material';

import { NgxSpinnerModule} from 'ngx-spinner'




//Componentes
  //Carga de Layout 
import { LayoutModule } from 'src/app/layout/layout.module';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { AppRoutingModule } from '../app-routing.module';

import { SolicitudesListComponent } from './solicitudesComp/solicitudes-list/solicitudes-list.component';
import { SolicitudesDetalleComponent } from './solicitudesComp/solicitudes-detalle/solicitudes-detalle.component';
import { SolicitudNuevaComponent } from './solicitud-nueva/solicitud-nueva.component';

//IPR Root Variables
import { rootGlobals } from '../rootGlobals';


//Servicios Solicitudes
import { ComentsService } from '../SolicitudesFianzas/services/coments.service';


import { RouterModule } from '@angular/router';






@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    //Shared View
    LayoutModule,
    //DateRange OK
    MyDateRangePickerModule,
    //DataView PrimeNg
    PanelModule,
    DataViewModule,
    //DataTable PrimeNg
    DataTableModule,
    SharedModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    CalendarModule,
    DropdownModule,
    ConfirmDialogModule,
    //Table PrimeNg
    TableModule,
    OrderListModule,
    StepsModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatTabsModule,
    MatCardModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    NgxSpinnerModule
    
    
  ],
  declarations: [
    SolicitudesComponent,
    SolicitudesListComponent,
    SolicitudesDetalleComponent,
    SolicitudNuevaComponent
  ],
  entryComponents: [
    SolicitudesDetalleComponent
  ],
   //IPR Services
   providers: [
    ComentsService,
    ConfirmationService,
    //Root Variables
    rootGlobals],

    exports: [
      //MatTableModule,
      //MatFormFieldModule,
      //MatInputModule
    ]
})
export class SolicitudesFianzasModule { }
