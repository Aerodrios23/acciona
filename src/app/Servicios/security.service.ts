import { Injectable } from '@angular/core';

//IPR 
import { HttpClient, HttpResponse, HttpHeaders, HttpParams, HttpErrorResponse,} from '@angular/common/http';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/internal/Observable';
import { map, catchError, tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { BlockUIService } from './block-uiservice.service';

import { SessionStorageService, SessionStorage, StorageService } from 'angular-web-storage';

import {JsonConvert, OperationMode, ValueCheckingMode} from "json2typescript"

import { InfoUsuario } from '../Models/infoUser'
import { Usuario } from '../Models/usuFront';
import { SchemaUsuario } from '../Models/shemaUsu';
import { FuncUsuario } from '../Models/funcUsu';
import { rootGlobals}  from 'src/app/rootGlobals'


@Injectable({
  providedIn: 'root'
})


export class SecurityService {

  //SessionStorage
  clientId: string = "vidaApp";
  correo: string = "administradorfinanzas@ordas.com.mx";
  contrasena: string = "prueba1234";
  userLog: string = "{'username':'ivancali23@gmail.com','fecha':'2019-01-25T17:32:25.164Z','modulo':'login','browser':'chrome','device':'unknown','isDesktop':true,'isMobile':false,'isTablet':false,'os':'windows'}";
  
  //añadir al userLog
  //this.deviceInfo = this.deviceService.getDeviceInfo();

  public token: string;
  public prueba: string;
  private urlLogin: string = "Security/Token";
  //private endpoint: string = "http://localhost:60961/api/Security/Token";
  //private endpoint: string = "http://aplicaciones.grupordas.com.mx/VidaWebApis/api/Security/Token";
  
  private jsonCrt: JsonConvert = new JsonConvert();
 
  constructor(
    private http: HttpClient,
    private blockUIService: BlockUIService,
    public session: SessionStorageService,
    private configVar: rootGlobals
  ) { }

  //*********************************************************************************************************************/

  loginuser(username: string, password: string): Observable<any> {
   
    let usuFront = new Usuario ();
    let defUser = new InfoUsuario();
    let shemUser = new SchemaUsuario();
    let jsonCrt = new JsonConvert();

    jsonCrt.operationMode = OperationMode.LOGGING; // print some debug data
    jsonCrt.ignorePrimitiveChecks = false; // don't allow assigning number to string etc.
    jsonCrt.valueCheckingMode = ValueCheckingMode.DISALLOW_NULL; // never allow null

    let headers: HttpHeaders = new HttpHeaders();

    const body = new HttpParams() 
    .set('grant_type', 'password') 
    .set('username', username)
    .set('password', password)
    .set('client_id', this.clientId)
    .set('userLog', this.userLog);
    
    return this.http
      .post<Usuario>(
        //this.endpoint, 
        this.configVar.endpoint + this.urlLogin,
        body.toString(), 
        { headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'), }
      )
      .pipe(map(data => data));
  
  }

  //*********************************************************************************************************************/

    private handleError(error: any, blockUIService: BlockUIService, blocking: Boolean) {

    let body = error.json();

    if (blocking) {
        blockUIService.blockUIEvent.emit({
            value: false
        });
    }
    return Observable.throw(body);

    }

    private parseResponse(response: Response, blockUIService: BlockUIService, blocking: Boolean) {

        let authorizationToken = response.headers.get("Authorization");

        if (authorizationToken != null) {
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("VIDA.Token", authorizationToken);
            }
        }

        if (blocking) {
            blockUIService.blockUIEvent.emit({
                value: false
            });
        }

        let body = response.json();

        return body;
    }
 

}

  